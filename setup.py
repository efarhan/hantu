import sys 
from cx_Freeze import setup, Executable 
build_exe_options = {"includes":["numbers","re"], "excludes":["sfml"]} 
base = None 

setup( 
	name = "main", 
	version = "1.0", 
	description = "Final Move, game by Team KwaKwa!", 
	options = {"build_exe": build_exe_options}, 
	executables = [Executable("FinalMove.py", base=base)])