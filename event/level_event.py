from engine.const import log, CONST
from engine.level_manager import get_level
from engine.loop import get_screen
from engine.vector import Vector2
from event.event_engine import Event
from game_object.text import Text
from json_export.json_main import get_element

__author__ = 'efarhan'


class LoopEvent(Event):
    def __init__(self, loop):
        Event.__init__(self)
        self.loop = loop

    def execute(self):
        gamestate_loop_event = get_level().loop_event
        if self.next_event and not self.clicked and gamestate_loop_event is None:
            get_level().loop_event = self.next_event
            self.clicked = not self.loop
    @staticmethod
    def parse_event(event_dict,obj=None):
        loop = get_element(event_dict, "loop")
        if loop is None:
            loop = False
        return LoopEvent(loop)
    def unclick(self):
        pass


class TimerEvent(Event):
    def __init__(self,frame=0,time=0):
        Event.__init__(self)
        self.time = time*CONST.framerate+frame
        self.counter = 0

    def execute(self):
        if self.counter == self.time:
            self.clicked = True
            Event.execute(self)
        else:
            self.counter += 1

    def unclick(self):
        self.counter = 0
        Event.unclick(self)

    @staticmethod
    def parse_event(event_dict,obj=None):
        time = get_element(event_dict,"time")
        if time is None:
            time = 0
        frame = get_element(event_dict,"frame")
        if frame is None:
            frame = 0
        return TimerEvent(frame,time)


class EndLoopEvent(Event):
    def __init__(self):
        Event.__init__(self)

    def execute(self):
        get_level().remove_event_loop()
    def unclick(self):
        return False

    @staticmethod
    def parse_event(event_dict,obj=None):
        return EndLoopEvent()


class TextGradientEvent(Event):
    def __init__(self,
                 pos=Vector2(),
                 size=10,
                 font="data/font/pixel_arial.ttf",
                 text="",
                 center=False,
                 relative=True,
                 gradient=0,
                 color=(0, 0, 0),
                 counter=2*CONST.framerate,obj=None):
        Event.__init__(self)
        if not relative:
            pos = obj.pos+pos
            log(pos)
        self.text = Text(pos,size,font,text,center=center,relative=relative,color=color,gradient=gradient)
        self.ttc = counter
        self.counter = counter
        self.clicked = False
        self.obj = obj

    def execute(self):
        #log(self.counter)
        if not self.clicked:
            self.text.loop(get_screen(), get_level().screen_pos)
            if self.text.time == self.text.gradient:
                if self.counter > 0:
                    self.counter -= 1
                else:
                    self.clicked = True
                    Event.execute(self)
        else:
            Event.execute(self)
    def change_text(self,text):
        self.text.change_text(text)
        self.clicked = False
    def unclick(self):
        Event.unclick(self)
        self.text.time = 1
        self.counter = self.ttc

    @staticmethod
    def parse_event(event_dict,obj=None):
        pos=get_element(event_dict,"pos")
        if pos is None:
            pos = Vector2()
        else:
            pos = Vector2(pos)
        size=get_element(event_dict,"size")
        font=get_element(event_dict,"font")
        text=get_element(event_dict,"text")
        center=get_element(event_dict,"center")
        if center is None:
            center = False
        relative=get_element(event_dict,"relative")
        if relative is None:
            relative = True
        gradient=get_element(event_dict,"gradient")
        if gradient is None:
            gradient = CONST.framerate
        else:
            gradient *= CONST.framerate
        color=get_element(event_dict,"color")
        if color is None:
            color = (0,0,0)
        counter = get_element(event_dict,"counter")
        if counter is None:
            counter = 2*CONST.framerate
        else:
            counter *= CONST.framerate
        return TextGradientEvent(pos,size,font,text,center,relative,gradient,color,counter,obj)



class VisualEvent(Event):
    def __init__(self,gamestate,name="",names=[],pos=None,next_pos=None,size=1):
        self.gamestate = gamestate
        self.name = name
        self.names = names


        self.pos = pos
        self.next_pos = next_pos
        self.size = size
        Event.__init__(self)

    def change(self,names=[]):
        for name in names:

            self.gamestate.characters[name].index = self.size
            if self.pos:
                self.gamestate.characters[name].pos = self.pos
            if self.next_pos:
                self.gamestate.characters[name].next_pos = self.next_pos
            self.gamestate.characters[name].update_rect()

    def execute(self):
        if self.name != "":
            self.change([self.name])
        elif self.names != []:
            self.change(self.names)
        Event.execute(self)

    @staticmethod
    def parse_event(event_dict,obj=None):
        return Event.parse_event(event_dict)


class SwitchEvent(Event):
    def __init__(self,gamestate,new_level_name):
        Event.__init__(self)
        self.gamestate = gamestate
        self.filename = new_level_name

    def execute(self):
        if not self.clicked:
            self.gamestate.switch(self.filename)
        Event.execute(self)

    @staticmethod
    def parse_event(event_dict,obj=None):
        new_level_name = get_element(event_dict,"name")
        if new_level_name:
            return SwitchEvent(get_level(),new_level_name)
        else:
            log("Invalid arg name for SwitchEvent")

