"""
Created on Feb 26, 2014

@author: efarhan
"""
from engine.const import CONST

from engine.vector import Vector2


if CONST.render == 'sfml':
    import sfml


def get_mouse():
    """
    Return mouse state as
    position, (left, right,middle)
    """
    if CONST.render == 'sfml':
        from engine.loop import get_screen
        from engine.init import get_screen_size
        mouse_pos = Vector2(sfml.Mouse.get_position(get_screen()))
        screen_diff_ratio = get_screen_size()*Vector2(1.0,1.0)/Vector2(get_screen().size)
        mouse_pos *= screen_diff_ratio
        return mouse_pos, [sfml.Mouse.is_button_pressed(sfml.Mouse.LEFT),
                                           sfml.Mouse.is_button_pressed(sfml.Mouse.RIGHT),
                                           sfml.Mouse.is_button_pressed(sfml.Mouse.MIDDLE)]
    elif CONST.render == 'pookoo':
        return None, None


def show_mouse(show=True,screen=None):
    """Show/hide mouse"""

    if CONST.render == 'sfml':
        if screen is None:
            from engine.loop import get_screen
            get_screen().mouse_cursor_visible = False#todo change in production
        else:
            screen.mouse_cursor_visible = False
