from engine.const import log
from engine.level_manager import get_level
from engine.stat import get_value
from engine.vector import Vector2
from event.event_engine import Event
from json_export.json_main import get_element

__author__ = 'efarhan'


class SelectEvent(Event):
    def __init__(self, obj):
        Event.__init__(self)
        self.obj = obj

    def execute(self):
        if not self.clicked:
            level = get_level()
            log("PC obj: "+str(self.obj))
            log(level)
            log(level.player)
            log(level.player.anim)

            level.player.anim.current_object = self.obj
            log(level.player.anim.current_object)

            Event.execute(self)

    @staticmethod
    def parse_event(event_dict, obj=None):
        return SelectEvent(obj)


class SetBoundEvent(Event):
    def __init__(self,min_bound,max_bound,stat_id,transition=True):
        Event.__init__(self)
        self.min_bound = min_bound
        self.max_bound = max_bound
        self.stat_id = stat_id
        self.transition = transition
    def execute(self):
        self.clicked = True
        try:
            try:
                if self.transition:
                    get_value(self.stat_id).anim.set_new_min(self.min_bound)
                else:
                    get_value(self.stat_id).anim.min_bound = self.min_bound
            except AttributeError:
                get_value(self.stat_id).anim.min_bound = self.min_bound
            get_value(self.stat_id).anim.max_bound = self.max_bound
        except AttributeError:
            pass
        Event.execute(self)

    @staticmethod
    def parse_event(event_dict,obj=None):
        min_bound = get_element(event_dict,"min_bound")
        if min_bound is None:
            min_bound = Vector2()
        else:
            min_bound = Vector2(min_bound)
        max_bound = get_element(event_dict,"max_bound")
        if max_bound is None:
            max_bound = Vector2()
        else:
            max_bound = Vector2(max_bound)
        transition = get_element(event_dict,"transition")
        if transition is None:
            transition = True
        return SetBoundEvent(min_bound,max_bound,get_element(event_dict,"stat_id"),transition=transition)


class SetAnimStateEvent(Event):
    def __init__(self,stat_id,state):
        Event.__init__(self)
        self.stat_id = stat_id
        self.state = state

    def execute(self):
        self.clicked = True
        get_value(self.stat_id).anim.state = self.state
        Event.execute(self)

    @staticmethod
    def parse_event(event_dict,obj=None):
        return SetAnimStateEvent(get_element(event_dict,"stat_id"),get_element(event_dict,"state"))