__author__ = 'efarhan'

from engine.init import get_screen_size
from animation.animation_main import Animation
from engine.const import CONST
from engine.physics import move, get_body_position
from engine.vector import Vector2


class CatAnimation(Animation):
    def __init__(self,obj):
        Animation.__init__(self,obj)
        self.move = 0
        self.dest_pos = Vector2()
        self.obj = obj

        self.min_bound = Vector2()
        self.max_bound = Vector2()
        self.direction = True
        self.speed = 10
        self.state = "still_right"

    def change_anim_freq(self,new_anim_freq,index=True):
        self.anim_freq = new_anim_freq
        self.anim_counter = 0
        if index:
            self.index = 0

    def update_animation(self, state="", invert=False):
        if (self.obj.pos.x > self.max_bound.x) and self.obj.pos.x > self.min_bound.x:
            if 'still' in self.state:
                self.change_anim_freq(0)
            else:
                self.anim_freq = 3
            self.direction = False
            self.state = 'move_left'
            move(self.obj.body, -self.speed)
        elif (self.obj.pos.x < self.min_bound.x) and self.obj.pos.x < self.max_bound.x:
            if 'still' in self.state:
                self.change_anim_freq(0)
            else:
                self.anim_freq = 5
            self.direction = True
            self.state = 'move_right'
            move(self.obj.body, self.speed)
        else:
            if self.index == 0:
                if 'still' not in self.state:
                    self.change_anim_freq(0)
                else:
                    self.anim_freq = CONST.framerate*3
            else:
                self.anim_freq = 5
            if self.direction:
                self.state = 'still_right'
            else:
                self.state = 'still_left'
            move(self.obj.body, 0)

        physics_pos = get_body_position(self.obj.body)

        if physics_pos:
            pos = physics_pos-self.obj.size/2
        else:
            pos = (self.obj.pos[0],self.obj.pos[1])
        if self.obj.screen_relative_pos:
            pos = pos-self.obj.screen_relative_pos*get_screen_size()
        self.obj.pos = pos
        Animation.update_animation(self,self.state,invert)