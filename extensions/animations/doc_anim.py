from animation.animation_main import Animation
from engine.const import CONST, log
from engine.init import get_screen_size
from engine.level_manager import get_level
from engine.physics import move, get_body_position
from engine.vector import Vector2
from json_export.event_json import parse_event_json

__author__ = 'efarhan'


class DocAnimation(Animation):
    def __init__(self,obj):
        Animation.__init__(self,obj)
        self.move = 0
        self.dest_pos = Vector2()
        self.obj = obj

        self.min_bound = Vector2()
        self.max_bound = Vector2()
        self.direction = True
        self.state = 'listen'
        self.speed = 2
        self.dialog_event = parse_event_json("data/json/hospital/doc_dad_dialog.json", obj=self.obj)
        self.trigger = False

    def change_anim_freq(self,new_anim_freq,index=True):
        self.anim_freq = new_anim_freq
        self.anim_counter = 0
        if index:
            self.index = 0

    def update_animation(self, state="", invert=False):
        #log(str(self.obj.pos.get_tuple())+" "+str(self.min_bound.get_tuple())+" "+str(self.max_bound.get_tuple())+
        #" "+str((self.obj.pos.x < self.min_bound.x))+" "+str(self.obj.pos.x < self.max_bound))
        if self.trigger:
            if not get_level().loop_event and not self.dialog_event.clicked:
                self.dialog_event.execute()

        if (self.obj.pos.x > self.max_bound.x) and self.obj.pos.x > self.min_bound.x:
            move(self.obj.body, -self.speed)
        elif (self.obj.pos.x < self.min_bound.x) and self.obj.pos.x < self.max_bound.x:
            if 'move' not in self.state:
                self.change_anim_freq(0)
            else:
                self.anim_freq = 15
            self.direction = True
            self.state = 'move_right'
            move(self.obj.body, self.speed)
        else:
            if 'move' in self.state:
                self.trigger = True
                self.change_anim_freq(0)
                self.state = 'listen'
            move(self.obj.body, 0)
        physics_pos = get_body_position(self.obj.body)

        if physics_pos:
            pos = physics_pos-self.obj.size/2
        else:
            pos = (self.obj.pos[0],self.obj.pos[1])
        if self.obj.screen_relative_pos:
            pos = pos-self.obj.screen_relative_pos*get_screen_size()
        self.obj.pos = pos
        Animation.update_animation(self,self.state,invert)