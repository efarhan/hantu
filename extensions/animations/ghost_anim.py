from animation.animation_main import Animation
from animation.player_animation import PlayerAnimation
from engine.const import log, CONST
from engine.init import get_screen_size
from engine.level_manager import get_level
from engine.loop import get_screen
from engine.physics import get_body_position, check_collision, move
from engine.stat import get_value, set_value
from engine.vector import Vector2
from event.event_main import get_button
from event.mouse_event import get_mouse
from event.physics_event import get_physics_event
from game_object.image import AnimImage, Image
from json_export.event_json import parse_event_json

__author__ = 'efarhan'


class GhostAnimation(PlayerAnimation):
    def __init__(self, player):
        PlayerAnimation.__init__(self,player)
        self.mouse_pressed = None
        self.mouse_clicked = False
        self.mouse_pos = None
        self.previous_mouse_pos = None
        self.current_object = None

        """Setup mouse_cursor"""
        self.mouse_cursor = AnimImage()
        self.mouse_cursor.screen_relative = True
        self.mouse_cursor.pos = Vector2()
        self.mouse_cursor.size = Vector2(120,120)
        self.mouse_cursor.permanent = True
        self.mouse_cursor.anim = Animation(None)
        self.mouse_cursor.anim.path = "data/sprites/"
        self.mouse_cursor.anim.path_list = ["mouse/"]
        self.mouse_cursor.anim.state_range = {
            "default": [1,2],
            "contact": [0,1]
        }
        self.mouse_cursor.anim.load_images()
        self.mouse_cursor.anim.anim_freq = 3
        self.mouse_cursor.anim.state = "default"
        get_level().objects[4].append(self.mouse_cursor)

        self.min_bound = Vector2()
        self.max_bound = Vector2(get_screen_size().x*5,0)
        self.bound_margin = 150
        self.transition = False
        self.delta_trans = 0.51

        self.chess_checkmate_event = parse_event_json("data/json/hospital/chess_checkmate.json")
        self.message_check_event = parse_event_json("data/json/hospital/message_check.json")
        self.message_click = False
    def change_anim_freq(self,new_anim_freq,index=True):
        self.anim_freq = new_anim_freq
        self.anim_counter = 0
        if index:
            self.index = 0

    def update_state(self):
        RIGHT = get_button('player_right')
        LEFT = get_button('player_left')

        horizontal = RIGHT-LEFT


        if self.transition:
            player_pos = self.player.pos+self.player.screen_relative_pos*get_screen_size()
            if player_pos.x > self.min_bound.x:
                self.min_bound -= get_screen_size()*Vector2(self.delta_trans,0)
                self.transition = False

        self.mouse_pos, self.mouse_pressed = get_mouse()
        
        self.mouse_cursor.pos = self.mouse_pos-self.mouse_cursor.size/2
        log(self.mouse_cursor.pos.get_tuple())
        #todo: check if contact
        objects = get_level().objects
        self.mouse_cursor.anim.state = "default"
        for index, layer in enumerate(objects):
            for image in layer:

                if 4 > index >= 2 and \
                                image != self.player and \
                                image != self.mouse_cursor and \
                                image.check_click(self.mouse_cursor.pos+self.mouse_cursor.size/2, get_level().screen_pos):
                        self.mouse_cursor.anim.state = "contact"

        if get_value("messageView"):
            if not self.mouse_pressed[0] and not self.mouse_clicked and not self.message_click:
                self.message_click = True
            if self.mouse_pressed[0] and self.message_click:
                self.message_check_event.execute()

        if self.mouse_pressed[0] and not self.mouse_clicked:
            self.mouse_clicked = True
        elif self.mouse_pressed[0] and self.mouse_clicked:
            #move the selected object if one
            if self.current_object:
                self.current_object.move(self.mouse_pos-self.previous_mouse_pos)
        else:
            self.mouse_clicked = False
            self.current_object = None
        if get_value("chessView"):
            if not self.mouse_clicked and check_collision(get_value("towerPiece").body,get_value("checkmate").body):
                self.chess_checkmate_event.execute()

        x_pos = (self.player.pos+self.player.screen_relative_pos*get_screen_size()).x

        if (horizontal == -1 or x_pos > self.max_bound.x) and x_pos > self.min_bound.x + self.bound_margin:
            if 'still' in self.state:
                self.change_anim_freq(0)
            else:
                self.anim_freq = 15
            self.direction = False
            self.state = 'move_left'
            move(self.player.body, -self.speed)
        elif (horizontal == 1 or x_pos < self.min_bound.x) and x_pos < self.max_bound.x - self.bound_margin:
            if 'still' in self.state:
                self.change_anim_freq(0)
            else:
                self.anim_freq = 15
            self.direction = True
            self.state = 'move_right'
            move(self.player.body, self.speed)
        else:
            if self.index == 0:
                if 'still' not in self.state:
                    self.change_anim_freq(0)
                else:
                    self.anim_freq = CONST.framerate*3
            else:
                self.anim_freq = 5
            if self.direction:
                self.state = 'still_right'
            else:
                self.state = 'still_left'
            move(self.player.body, 0)

        self.previous_mouse_pos = self.mouse_pos

        physics_pos = get_body_position(self.player.body)

        if physics_pos:
            pos = physics_pos-self.player.size/2
        else:
            pos = (self.player.pos[0],self.player.pos[1])
        if self.player.screen_relative_pos:
            pos = pos-self.player.screen_relative_pos*get_screen_size()
        self.player.pos = pos

    def set_new_min(self, min_bound):
        self.min_bound = min_bound+get_screen_size()*Vector2(self.delta_trans, 0)
        self.transition = True

    def get_screen_pos(self):
        '''if self.player.screen_relative_pos.x*get_screen_size().x+self.player.pos.x > 0.5*get_screen_size().x-self.size.x/2:
            return self.player.pos
        else:
            self.player.screen_relative_pos += self.player.pos/get_screen_size()
            self.player.pos = Vector2()
            return Vector2()
        '''

        if get_value("chessView"):
            return Vector2(11000, 0)
        if get_value("messageView"):
            return Vector2(12920, 0)
        player_pos = self.player.screen_relative_pos*get_screen_size()+self.player.pos
        #log(str(self.player.pos.get_tuple())+" "+str(self.player.screen_relative_pos.get_tuple()))
        screen_pos = None
        if not self.transition and self.min_bound.x < player_pos.x < 0.5*get_screen_size().x+self.min_bound.x:
            #log("MIN"+str(self.player.pos.get_tuple())+" "+str(self.player.screen_relative_pos.get_tuple()))
            self.player.screen_relative_pos = (player_pos-self.min_bound)/get_screen_size()
            self.player.pos = self.min_bound
            screen_pos = self.min_bound
        elif self.max_bound.x > player_pos.x > -0.5*get_screen_size().x+self.max_bound.x:
            #log("MAX"+str(self.player.pos.get_tuple())+" "+str(self.player.screen_relative_pos.get_tuple()))
            self.player.screen_relative_pos = \
                (player_pos-(self.max_bound-get_screen_size()*Vector2(1,0)))/get_screen_size()
            self.player.pos = self.max_bound-get_screen_size()*Vector2(1,0)
            screen_pos = self.max_bound-get_screen_size()*Vector2(1,0)
        else:
            #set screen_relative to 0.5
            speed = 0.005
            if self.player.screen_relative_pos.x > 0.5+speed*2:
                #log("DEF_BIG"+str(self.player.pos.get_tuple())+" "+str(self.player.screen_relative_pos.get_tuple()))
                self.player.screen_relative_pos -= Vector2(speed,0)
                self.player.pos+=Vector2(speed,0)*get_screen_size()
            elif self.player.screen_relative_pos.x < 0.5-speed*2:
                #log("DEF_LOW"+str(self.player.pos.get_tuple())+" "+str(self.player.screen_relative_pos.get_tuple()))
                self.player.screen_relative_pos += Vector2(speed,0)
                self.player.pos -= Vector2(speed,0)*get_screen_size()
            screen_pos = self.player.pos
        #log(screen_pos.get_tuple())
        return screen_pos
