import sfml
from engine.const import CONST
from engine.image_manager import show_image
from engine.init import get_screen_size
from engine.vector import Vector2

__author__ = 'efarhan'


class FadeScreen():
    def __init__(self,
                 color=(0,0,0),
                 alpha=0,
                 ttf=1):
        self.fadein = False
        self.fadeout = False
        self.alpha = alpha
        self.ttf = ttf #time to do 255 in seconds
        self.speed = 255 / (self.ttf*CONST.framerate)
        self.color = color
        if CONST.render == 'sfml':
            self.fade_screen = sfml.RectangleShape()
            self.fade_screen.size = get_screen_size().get_tuple()
            self.fade_screen.fill_color = sfml.Color(self.color[0],
                                                     self.color[1],
                                                     self.color[2],
                                                     self.alpha)


    def set_ttf(self,ttf):
        self.ttf = ttf #time to do 255 in seconds
        self.speed = 255 / (self.ttf*CONST.framerate)

    def loop(self, screen):
        if self.fadein:
            if self.alpha > 0:
                self.alpha -= self.speed
            if self.alpha <= 0:
                self.alpha = 0
                self.fadein = False
        if self.fadeout:
            if self.alpha < 255:
                self.alpha += self.speed
            if self.alpha >= 255:
                self.alpha = 255
                self.fadeout = False

        if CONST.render == 'sfml':
            self.fade_screen.fill_color = sfml.Color(self.color[0],self.color[1],self.color[2],self.alpha)
            show_image(self.fade_screen,screen,Vector2(),new_size=Vector2(screen.size))