from engine.image_manager import sanitize_img_manager
from game_object.fade_screen import FadeScreen


class Scene():
    def __init__(self):
        pass

    def init(self, loading=False):
        self.fade_screen = FadeScreen(color=(255,255,255))
        self.fade_screen.fadein = True
        self.fade_screen.alpha = 255

    def loop(self, screen):
        self.fade_screen.loop(screen)

    def exit(self):
        pass
        #sanitize_img_manager()