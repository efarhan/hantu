"""
Created on 9 dec. 2013

@author: efarhan
"""
from engine.level_manager import switch_level
from engine.sound_manager import update_music_status
from engine.stat import get_value
from engine.vector import Vector2


from levels.scene import Scene
from engine.const import log, CONST
from network.gamestate_network import client, NetworkGamestate
from json_export.level_json import load_level
from engine.physics import init_physics, update_physics, deinit_physics
from levels.editor import Editor
from engine.image_manager import fill_surface
from event.mouse_event import show_mouse, get_mouse


class GameState(Scene, Editor, NetworkGamestate):
    def __init__(self, filename):
        self.bg_color = [0, 0, 0]
        self.player = None
        self.event = {}
        self.current_event = None #to unclick
        self.filename = filename
        self.objects = None
        self.screen_pos = None
        self.lock = None
        self.click = None

        if CONST.debug:
            Editor.__init__(self)

    def init(self, loading=False):
        Scene.init(self)
        self.fade_screen.set_ttf(3.5)
        init_physics()
        self.objects = [[] for i in range(CONST.layers)]
        self.screen_pos = Vector2()
        self.show_mouse = False
        if self.filename != "":
            log("Loading level " + self.filename)
            if not load_level(self):
                from engine.level_manager import switch_level
                switch_level(Scene())
        self.lock = False
        self.click = [False,False,False]

        NetworkGamestate.init(self)
        self.loop_event = None
        if not loading:
            self.execute_event('on_init')

    def execute_event(self, name):
        #log(str(self.event[name]))
        try:
            if self.event[name]:
                self.event[name].execute()
        except KeyError as e:
            #log("Error: No such event: %s"%(name)+str(e), 1)
            pass

    def switch(self, newfilename):
        from levels.loading_screen import LoadingScreen
        self.filename = newfilename
        self.exit()
        switch_level(LoadingScreen(self.filename))

    def loop(self, screen):
        update_music_status()
        fill_surface(screen, self.bg_color[0], self.bg_color[1], self.bg_color[2], 255)

        '''Event
        If mouse_click on element, execute its event, or not null'''
        if self.show_mouse:
            show_mouse()
            mouse_pos, pressed = get_mouse()
            mouse_pressed = -1
            for i in range(len(pressed)):
                if pressed[i] and not self.click[i]:
                    mouse_pressed = i

            if mouse_pressed != -1:
                self.current_event = None
                self.click[0] = True
                for layer in self.objects:
                    for image in layer:
                        if image != self.player and \
                                image != self.player.anim.mouse_cursor and\
                                image.check_click(mouse_pos, self.screen_pos):

                            try:
                                if mouse_pressed == 0:
                                    self.current_event = image.event["left_click"]
                                elif mouse_pressed == 1:
                                    self.current_event = image.event["right_click"]
                            except KeyError:
                                pass
                            log("Clicked on: "+str(image)+" "+str(self.current_event))
                if self.current_event:
                    self.current_event.execute()
            elif not pressed[0]:
                self.click[0] = False
                if self.current_event and self.current_event.clicked:
                    self.current_event.unclick()

        if not self.lock:
            update_physics()


        '''Show images'''
        if self.player and self.player.anim:
            self.screen_pos = self.player.anim.get_screen_pos()
        remove_image = []
        for i, layer in enumerate(self.objects):
            if i == 2:
                NetworkGamestate.loop(self, screen)
            for j, img in enumerate(layer):
                img.loop(screen, self.screen_pos)
                if img.remove:
                    remove_image.append(img)
        for r in remove_image:
            self.objects[i].remove(r)

        self.execute_event("on_loop")
        event = self.loop_event
        if event:
            event.execute()
            last_event = event
            while last_event.get_next_event():
                last_event = last_event.get_next_event()
            if last_event.clicked:
                self.remove_event_loop()

        '''Editor'''
        if CONST.debug:
            Editor.loop(self, screen, self.screen_pos)
        Scene.loop(self,screen)
        if get_value("endLevel"):
            if self.fade_screen.alpha < 255:
                self.fade_screen.alpha+=1
            elif self.fade_screen.alpha == 255:
                from levels.credits import Credits
                switch_level(Credits())
    def remove_event_loop(self):
        self.loop_event.unclick()
        self.loop_event = None

    def exit(self):
        deinit_physics()

        Scene.exit(self)
