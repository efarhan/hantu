from animation.animation_main import Animation
from engine.const import CONST
from engine.image_manager import fill_surface, show_image
from engine.init import get_screen_size
from engine.level_manager import switch_level
from engine.sound_manager import load_sound, play_sound, check_sound, update_music_status, set_playlist, \
    check_music_status
from engine.vector import Vector2
from game_object.image import AnimImage, Image
from levels.gamestate import GameState

from levels.scene import Scene

__author__ = 'efarhan'


class TitleScreen(Scene):
    def __init__(self,loading_screen):
        Scene.__init__(self)
        self.loading_screen = loading_screen

    def init(self, loading=False):
        Scene.init(self,loading)

        anim_freq = 3

        self.kwakwa_present = AnimImage()
        self.kwakwa_present.pos = get_screen_size()/2
        self.kwakwa_present.size = Vector2(375,143)
        self.kwakwa_present.pos -= self.kwakwa_present.size/2
        self.kwakwa_present.anim = Animation(None)
        self.kwakwa_present.anim.path = "data/sprites/"
        self.kwakwa_present.anim.path_list = ["intro/01_teamKwakwa/"]
        self.kwakwa_present.anim.load_images()
        self.kwakwa_present.anim.anim_freq = anim_freq

        self.ambulance = AnimImage()
        self.ambulance.pos = Vector2(0,get_screen_size().y-353)
        self.ambulance.size = Vector2(1920,353)
        self.ambulance.anim = Animation(None)
        self.ambulance.anim.path = "data/sprites/"
        self.ambulance.anim.path_list = ["intro/02_ambulance/"]
        self.ambulance.anim.load_images()
        self.ambulance.anim.anim_freq = anim_freq

        self.title_game = AnimImage()
        self.title_game.pos = get_screen_size()/2
        self.title_game.size = Vector2(1194,381)
        self.title_game.pos -= self.title_game.size/2
        self.title_game.anim = Animation(None)
        self.title_game.anim.path = "data/sprites/"
        self.title_game.anim.path_list = ["intro/03_1_titre/"]
        self.title_game.anim.load_images()
        self.title_game.anim.anim_freq = anim_freq
        self.title_game_img = self.title_game.anim.img_indexes[-1]

        self.line = AnimImage()
        self.line.pos = Vector2(0,(self.title_game.pos+self.title_game.size).y)
        self.line.size = Vector2(1920,14)
        self.line.anim = Animation(None)
        self.line.anim.path = "data/sprites/"
        self.line.anim.path_list = ["intro/04_ligne/"]
        self.line.anim.load_images()
        self.line.anim.anim_freq = anim_freq
        self.line_img = self.line.anim.img_indexes[-1]

        self.fond_degrade = Image("data/sprites/intro/fond_degrade.png",Vector2())

        self.screen_pos = Vector2()
        if not loading:
            set_playlist(["data/sound/introb.ogg"])
        self.counter = 0

    def loop(self, screen):
        fill_surface(screen,0,0,0)
        if 173*2 <= self.counter <= 412*2:
            self.kwakwa_present.loop(screen,Vector2())
        if 601*2 <= self.counter <= 830*2:
            self.ambulance.loop(screen,Vector2())
        if 831*2 <= self.counter <= 1134*2:
            self.title_game.loop(screen,Vector2())
        elif self.counter > 1134*2:
            show_image(self.title_game_img,screen,self.title_game.pos-self.screen_pos/2,new_size=self.title_game.size)

        if 1144*2 <= self.counter <= 1253*2:
            self.line.loop(screen,Vector2())
        elif self.counter > 1253*2:
            self.screen_pos = Vector2(0,float(self.counter-1253*2)/(1384*2-1253*2)*(2*get_screen_size().y))
            show_image(self.line_img,screen,self.line.pos-self.screen_pos/2)
            self.fond_degrade.loop(screen,self.screen_pos)

        if self.counter == 0:
            self.fade_screen.fadein = True
        self.counter += 1
        if self.counter >= 1384*2:
            switch_level(self.loading_screen)
        Scene.loop(self,screen)
