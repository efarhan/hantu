'''
Created on 8 mars 2014

@author: efarhan
'''
from engine.vector import Vector2
from json_export.json_main import load_json, get_element
from levels.scene import Scene
from engine.init import get_screen_size
from engine.image_manager import fill_surface
from game_object.image import Image
from engine.sound_manager import play_music, check_music_status, set_playlist
from levels.gamestate import GameState
from engine.font_manager import load_font
from game_object.text import Text
#font_obj, msg, sound_obj


class Credits(Scene):
    def init(self):
        Scene.init(self)
        self.font = 'data/font/GeosansLight.ttf'
        self.size = 100
        credits_json = load_json("data/json/credits.json")
        self.texts = get_element(credits_json,"texts")
        for i in range(len(self.texts)):
            self.texts[i] = Text(Vector2(), self.size, self.font, self.texts[i], color=(0,0,0))
            self.texts[i].pos = Vector2(get_screen_size().x/2-self.texts[i].size.x/2,self.texts[i].pos.y+i*self.size)
        set_playlist(['data/music/credits.ogg'])
        self.count = -get_screen_size().y
        self.bg = Image("data/sprites/chess/chess.png",Vector2())
        self.cm = Image("data/sprites/checklist/checkmate_note_small.png",Vector2())
        self.cm.pos = (get_screen_size()-self.cm.size)*Vector2(1,0)
        self.thank = Text(get_screen_size()/2,100,self.font,"Thanks for playing our game",center=True)
        
    def loop(self, screen):
        fill_surface(screen,255,255,255)
        self.bg.loop(screen,Vector2())
        self.cm.loop(screen,Vector2())
        for i in range(len(self.texts)):
            self.texts[i].loop(screen, Vector2(0,self.count))
        self.count += 1

        if(self.count >= self.size*len(self.texts)):
            self.thank.loop(screen,Vector2())
            self.fade_screen.fadeout = True
            self.fade_screen.ttf = 3
        if check_music_status():
            from engine.level_manager import switch_level
            switch_level(0)

        Scene.loop(self,screen)
