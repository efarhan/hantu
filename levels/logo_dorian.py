from engine.vector import Vector2
from levels.scene import Scene
from engine.init import get_screen_size
from engine.image_manager import fill_surface
from game_object.image import Image
from engine.sound_manager import check_music_status, set_playlist
#font_obj, msg, sound_obj


class Dorian(Scene):
    def __init__(self,loading_screen):
        self.loading_screen = loading_screen

    def init(self):

        self.text = Image('data/sprites/text/logo-dorian_sred.png',get_screen_size() / 2)
        set_playlist(['data/sound/logo_dorian.ogg'])

    def loop(self, screen):
        fill_surface(screen, 255, 255, 255)
        self.text.pos = get_screen_size() / 2 - self.text.size / 2
        self.text.loop(screen, Vector2())

        if check_music_status():
            from engine.level_manager import switch_level
            switch_level(self.loading_screen)


			
